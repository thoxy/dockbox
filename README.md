# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/10784873/image.png" width="47" height="47" /> DockBox

With DockBox you can easily deploy a Multi User Seedbox that use Docker.

Based on cloudbox : [GitHub Link](https://github.com/Cloudbox/Cloudbox).

Special thanks to laster13 for his script : [Source](https://github.com/laster13/patxav).

# Prerequisites

1. A server on **Debian** or **Ubuntu**.
2. A **domain name**.
3. A Google Drive account if the Seedbox is oriented PlexDrive. **(Optional)**

# Apps

## System and apps Manage
- [**Traefik**](https://traefik.io/) - Proxy I/O web + Letsencrypt : [**( traefik )**](https://hub.docker.com/_/traefik)
- [**Watchtower**](https://github.com/v2tec/watchtower) - Update docker container : [**( v2tec/watchtower )**](https://hub.docker.com/r/v2tec/watchtower)
- [**Portainer**](https://www.portainer.io/) - Docker Manager Web UI : [**( portainer/portainer )**](https://hub.docker.com/r/portainer/portainer)
- [**Netdata**](https://github.com/netdata/netdata) - Real time Web UI for : [**( netdata/netdata )**](https://hub.docker.com/r/netdata/netdata) **

## Torrents Clients

- [**Rtorrent**](https://github.com/rakshasa/rtorrent) + [**Rutorrent**](https://github.com/Novik/ruTorrent) - Rtorrent + Rutorrent from xatz : [**( xataz/rtorrent-rutorrent )**](https://hub.docker.com/r/xataz/rtorrent-rutorrent)
- [**Flood**](https://github.com/jfurrow/flood) - Rtorrent + Flood : [**( kuni00/rtorrent )**](https://hub.docker.com/r/kuni00/rtorrent) ***
- [**Transmission**](https://transmissionbt.com/) - Transmission + Web UI : [**( linuxserver/transmission )**](https://hub.docker.com/r/linuxserver/transmission) *
- [**Deluge**](https://deluge-torrent.org/) - Deluge + Web UI : [**( linuxserver/deluge )**](https://hub.docker.com/r/linuxserver/deluge) *

## Media Server
- [**Plex**](https://www.plex.tv/fr/)  - Media Server from Plex : [**( plexinc/pms-docker )**](https://hub.docker.com/r/plexinc/pms-docker/])
- [**Emby**](https://hub.docker.com/r/emby/embyserver)  - Media Server from Emby : [**( emby/embyserver )**](https://hub.docker.com/r/emby/embyserver])
- [**Jellyfin**](https://github.com/jellyfin/jellyfin) - Media Server from Jellifin : [**( jellyfin/jellyfin )**](https://hub.docker.com/r/jellyfin/jellyfin) *

## File Manager
- [**Nextcloud**](https://nextcloud.com/) - Nextcloud File Manager : [**( wonderfall/nextcloud )**](https://hub.docker.com/r/wonderfall/nextcloud) ***
- [**FileBrowser**](https://filebrowser.xyz/) - Simple File Browser : [**( jellyfin/jellyfin )**](https://hub.docker.com/r/jellyfin/jellyfin) **
- [**CloudCommander**](http://cloudcmd.io/) - File Browser with mores avanced features : [**( jellyfin/jellyfin )**](https://hub.docker.com/r/jellyfin/jellyfin) *

## Direct Downloader
- [**Pyload**](https://pyload.net/) - Media Server from Jellifin : [**( writl/pyload )**](https://hub.docker.com/r/writl/pyload)
- [**Aria2**](https://aria2.github.io/) + [**WebUi**](https://github.com/ziahamza/webui-aria2) - Rtorrent + Rutorrent from xatz : [**( xujinkai/aria2-with-webui )**](https://hub.docker.com/r/xujinkai/aria2-with-webui) *

## NewsGroup Downloader
- [**Sabnzbd**](https://sabnzbd.org/) - Nzb file downloader : [**( linuxserver/sabnzbd )**](https://hub.docker.com/r/linuxserver/sabnzbd) ** ***
- [**Nzbget**](https://nzbget.net/) - Lightweight Nzb file downloader : [**( linuxserver/nzbget )**](https://hub.docker.com/r/linuxserver/nzbget) *


## Homepage ( Service Manager )
- [**Organizr**](https://github.com/causefx/Organizr) - Advanced Panel to organise link of all web applications : [**( tronyx/docker-organizr-v2 )**](https://hub.docker.com/r/tronyx/docker-organizr-v2) ***
- [**Heimdall**](https://github.com/linuxserver/Heimdall) - Verry simple Panel to organise link of all web applications : [**( linuxserver/heimdall )**](https://hub.docker.com/r/linuxserver/heimdall) 

## Movie Automation
- [**Radarr**](https://github.com/Radarr/Radarr) - Sonarr is an automated movie NZB & Torrent searcher and snatcher : [**( linuxserver/radarr )**](https://hub.docker.com/r/linuxserver/radarr) 
- [**Couchpotato**](https://couchpota.to/) - Couchpotato is an automated movie NZB & Torrent searcher and snatcher : [**( linuxserver/couchpotato )**](https://hub.docker.com/r/linuxserver/couchpotato) *
- [**Watcher**](https://nosmokingbandit.github.io/) - Watcher is an automated movie NZB & Torrent searcher and snatcher : [**( linuxserver/watcher )**](https://hub.docker.com/r/linuxserver/watcher) 

## TV-Show Automation
- [**Sonarr**](https://sonarr.tv/) - TV-Show manager and download automation : [**( linuxserver/sonarr )**](https://hub.docker.com/r/linuxserver/sonarr) 
- [**Sickchill**](https://sickchill.github.io/) - TV-Show manager and download automation (Fork Sickrage) : [**( sickchill/sickchill )**](https://hub.docker.com/r/sickchill/sickchill) *
- [**Sickrage**](https://git.sickrage.ca/SiCKRAGE/sickrage) - TV-Show manager and download automation : [**( linuxserver/sickrage )**](https://hub.docker.com/r/linuxserver/sickrage) *
- [**DuckieTV**](http://schizoduckie.github.io/DuckieTV/) - TV-Show manager and download automation : [**( oskarirauta/docker-duckietv )**](https://hub.docker.com/r/oskarirauta/docker-duckietv) *
- [**Medusa**](https://pymedusa.com/) - TV-Show manager and download automation (Fork Sickrage-Sickchill) : [**( xataz/medusa )**](https://hub.docker.com/r/xataz/medusa) (maybe ***)

## Indexer
### Torrent
- [**Jackett**](https://github.com/Jackett/Jackett) - Search indexer for Multi Torrents Trackers (Public/Private) : [**( linuxserver/jackett )**](https://hub.docker.com/r/linuxserver/jackett) 
### Newsgroup
- [**NzbHydra2**](https://github.com/theotherp/nzbhydra2) - Search Indexer for Newsgroup Boards : [**( linuxserver/hydra2 )**](https://hub.docker.com/r/linuxserver/hydra2)

## Music
### Collection Manager and Automation
- [**Lidarr**](https://lidarr.audio/) - Music manager and download automation : [**( linuxserver/lidarr )**](https://hub.docker.com/r/linuxserver/lidarr)
### Stream
- [**Subsonic**](http://www.subsonic.org/) - Music stream server (Like MPT): [**( hurricane/subsonic )**](https://hub.docker.com/r/hurricane/subsonic)

## Misc 
- [**Tautulli**](https://tautulli.com/) - Dashboard of Statistics for Plex : [**( tautulli/tautulli )**](https://hub.docker.com/r/tautulli/tautulli) (maybe ***)
- [**Jorum**](https://github.com/dyslexicjedi/Jorum) - Dashboard of Statistics for Emby : [**( dyslexicjedi/jorum )**](https://hub.docker.com/r/dyslexicjedi/jorum) *
- [**Ombi**](https://ombi.io/) - Plex Request and User Management System : [**( linuxserver/ombi )**](https://hub.docker.com/r/linuxserver/ombi)

**And more on the way ...**

# Install (need root)

```bash
sudo su -
apt update && apt upgrade -y
apt install git
git clone https://github.com/laster13/patxav.git /opt/seedbox-compose
cd /opt/seedbox-compose 
./seedbox.sh
```

# Knowed Issues
1. Plex CLAIM input not work

- "*" need to add
- "**" need to work (not finished)
- "***" work but need change repo