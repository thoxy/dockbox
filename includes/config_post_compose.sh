#!/bin/bash

function config_post_compose() {
for line in $(cat $SERVICESPERUSER);
do
		if [[ "$line" == "plex" ]]; then
			echo -e "${BLUE}### CONFIG POST COMPOSE PLEX ###${NC}"
			echo -e " ${BWHITE}* Processing plex config file...${NC}"
			PLEXDRIVE="/usr/bin/plexdrive"
			cd /home/$SEEDUSER
			# CLAIM pour Plex
			echo ""
			echo -e " ${BWHITE}* Un token est nécéssaire pour AUTHENTIFIER le serveur Plex ${NC}"
			echo -e " ${BWHITE}* Pour obtenir un identifiant CLAIM, allez à cette adresse et copier le dans le terminal ${NC}"
			echo -e " ${CRED}* https://www.plex.tv/claim/ ${CEND}"
			echo ""
			read -rp "CLAIM = " CLAIM
			if [ -n "$CLAIM" ]
			then
				sed -i "s|%CLAIM%|$CLAIM|g" /home/$SEEDUSER/docker-compose.yml
			fi
			checking_errors $?
			echo ""
			if [[ -e "$PLEXDRIVE" ]]; then
				plex_sections
				touch /home/$SEEDUSER/scripts/plex_autoscan/plex_autoscan_rutorrent.sh
				touch /home/$SEEDUSER/scripts/plex_autoscan/plex_autoscan_flood.sh
				PORT=$(grep SERVER_PORT /home/$SEEDUSER/scripts/plex_autoscan/config/config.json | cut -d ':' -f2 | sed 's/, //' | sed 's/ //')
				PLEXCANFILE="/home/$SEEDUSER/scripts/plex_autoscan/plex_autoscan_rutorrent.sh"
				PLEXCANFLOODFILE="/home/$SEEDUSER/scripts/plex_autoscan/plex_autoscan_flood.sh"

				cat "$BASEDIR/includes/config/plex_autoscan/plex_autoscan_start.sh" > $PLEXCANFILE
				cat "$BASEDIR/includes/config/plex_autoscan/plex_autoscan_start.sh" > $PLEXCANFLOODFILE
				chmod 755 $PLEXCANFILE
				chmod 755 $PLEXCANFLOODFILE

				for line in $(cat $INSTALLEDFILE);
				do
					NOMBRE=$(sed -n "/$SEEDUSER/=" $CONFDIR/users)
					if [ $NOMBRE -le 1 ] ; then
						ACCESSDOMAIN=$(grep plex $INSTALLEDFILE | cut -d\- -f3)
					else
						ACCESSDOMAIN=$(grep plex $INSTALLEDFILE | cut -d\- -f3-4)
					fi
				done

				sed -i "s|%ACCESSDOMAIN%|$ACCESSDOMAIN|g" $PLEXCANFILE
				sed -i -e "s/%ANIMES%/${ANIMES}/g" $PLEXCANFILE
				sed -i -e "s/data/mnt/g" $PLEXCANFILE
				sed -i -e "s/%FILMS%/${FILMS}/g" $PLEXCANFILE
				sed -i -e "s/%SERIES%/${SERIES}/g" $PLEXCANFILE
				sed -i -e "s/%MUSIC%/${MUSIC}/g" $PLEXCANFILE
				sed -i -e "s/%PORT%/${PORT}/g" $PLEXCANFILE

				sed -i "s|%ACCESSDOMAIN%|$ACCESSDOMAIN|g" $PLEXCANFLOODFILE
				sed -i -e "s/%ANIMES%/${ANIMES}/g" $PLEXCANFLOODFILE
				sed -i -e "s/%FILMS%/${FILMS}/g" $PLEXCANFLOODFILE
				sed -i -e "s/%SERIES%/${SERIES}/g" $PLEXCANFLOODFILE
				sed -i -e "s/%MUSIC%/${MUSIC}/g" $PLEXCANFLOODFILE
				sed -i -e "s/%PORT%/${PORT}/g" $PLEXCANFLOODFILE

				grep -R "rtorrent" "$INSTALLEDFILE" > /dev/null 2>&1
				if [[ "$?" == "0" ]]; then
					docker exec -t rtorrent-$SEEDUSER sed -i 's/\<unsorted=y\>/& "exec=\/scripts\/plex_autoscan\/plex_autoscan_rutorrent.sh"/' /usr/local/bin/postdl
				fi
				
				POSTDL="/home/$SEEDUSER/docker/flood/filebot/postdl"
				if [[ -e "$POSTDL" ]]; then
				docker exec -t flood-$SEEDUSER sed -i 's/\<unsorted=y\>/& "exec=\/scripts\/plex_autoscan\/plex_autoscan_flood.sh"/' /usr/local/bin/postdl
				fi
			fi
		fi

		if [[ "$line" == "subsonic" ]]; then
		echo -e "${BLUE}### CONFIG POST COMPOSE SUBSONIC ###${NC}"
		echo -e " ${BWHITE}* Mise à jour subsonic...${NC}"
		docker exec subsonic-$SEEDUSER update > /dev/null 2>&1
		docker restart subsonic-$SEEDUSER > /dev/null 2>&1
		docker exec subsonic-$SEEDUSER bash -c "echo '127.0.0.1 localhost.localdomain localhost subsonic.org' >> /etc/hosts"
		checking_errors $?
		echo ""
		echo -e "${BLUE}### SUBSONIC PREMIUM ###${NC}"
		echo -e "${BWHITE}	--> foo@bar.com${NC}"
		echo -e "${BWHITE}	--> f3ada405ce890b6f8204094deb12d8a8${NC}"
		echo ""
		fi

		if [[ "$line" == "rtorrent" ]]; then
			replace_media_compose
			echo -e "${BLUE}### CONFIG POST COMPOSE FILEBOT RUTORRENT ###${NC}"
			echo -e " ${BWHITE}* Mise à jour filebot rutorrent...${NC}"
						
			docker exec -t rtorrent-$SEEDUSER sed -i -e "s/Movies/${FILMS}/g" /usr/local/bin/postdl
			docker exec -t rtorrent-$SEEDUSER sed -i -e "s/TV/${SERIES}/g" /usr/local/bin/postdl
			docker exec -t rtorrent-$SEEDUSER sed -i -e "s/Music/${MUSIC}/g" /usr/local/bin/postdl
			docker exec -t rtorrent-$SEEDUSER sed -i -e "s/Anime/${ANIMES}/g" /usr/local/bin/postdl
			docker exec -t rtorrent-$SEEDUSER sed -i -e "s/data/mnt/g" /usr/local/bin/postdl
			docker exec -t rtorrent-$SEEDUSER sed -i '/*)/,/;;/d' /usr/local/bin/postdl
			checking_errors $?
			grep -R "plex" "$INSTALLEDFILE" > /dev/null 2>&1
			if [[ "$?" == "0" ]]; then
				docker exec -t rtorrent-$SEEDUSER sed -i 's/\<unsorted=y\>/& "exec=\/scripts\/plex_autoscan\/plex_autoscan_rutorrent.sh"/' /usr/local/bin/postdl
			fi
			echo ""
		fi

		if [[ "$line" == "filebrowser" ]]; then
            mkdir -p "/home/$SEEDUSER/docker/filebrowser" && cp "./config/filebrowser/filebrowser.json" $_
			echo ""
		fi

		if [[ "$line" == "flood" ]]; then
			replace_media_compose
			echo -e "${BLUE}### CONFIG POST COMPOSE FILEBOT FLOOD ###${NC}"
			var="Mise à jour filebot flood, patientez..."
			decompte 15

			touch /home/$SEEDUSER/docker/flood/filebot/postrm
			touch /home/$SEEDUSER/docker/flood/filebot/postdl

			POSTRM="/home/$SEEDUSER/docker/flood/filebot/postrm"
			POSTDL="/home/$SEEDUSER/docker/flood/filebot/postdl"

			cat "$BASEDIR/includes/config/flood/postrm" > $POSTRM
			cat "$BASEDIR/includes/config/flood/postdl" > $POSTDL

			echo 'system.method.set_key=event.download.finished,filebot,"execute={/usr/local/bin/postdl,$d.get_base_path=,$d.get_name=,$d.get_custom1=}"' >> /home/$SEEDUSER/docker/flood/config/rtorrent/rtorrent.rc
			echo 'system.method.set_key=event.download.erased,filebot_cleaner,"execute=/usr/local/bin/postrm"' >> /home/$SEEDUSER/docker/flood/config/rtorrent/rtorrent.rc

			FILEBOT_RENAME_METHOD=$(grep FILEBOT_RENAME_METHOD /home/$SEEDUSER/docker-compose.yml | cut -d '=' -f2 | head -n 1)
			FILEBOT_RENAME_MOVIES=$(grep FILEBOT_RENAME_MOVIES /home/$SEEDUSER/docker-compose.yml | cut -d '=' -f2 | head -n 1)
			FILEBOT_RENAME_MUSICS=$(grep FILEBOT_RENAME_MUSICS /home/$SEEDUSER/docker-compose.yml | cut -d '=' -f2 | head -n 1)
			FILEBOT_RENAME_SERIES=$(grep FILEBOT_RENAME_SERIES /home/$SEEDUSER/docker-compose.yml | cut -d '=' -f2 | head -n 1)
			FILEBOT_RENAME_ANIMES=$(grep FILEBOT_RENAME_ANIMES /home/$SEEDUSER/docker-compose.yml | cut -d '=' -f2 | head -n 1)

    			sed -e 's#<FILEBOT_RENAME_MOVIES>#'"$FILEBOT_RENAME_MOVIES"'#' \
        		    -e 's#<FILEBOT_RENAME_METHOD>#'"$FILEBOT_RENAME_METHOD"'#' \
        		    -e 's#<FILEBOT_RENAME_MUSICS>#'"$FILEBOT_RENAME_MUSICS"'#' \
        		    -e 's#<FILEBOT_RENAME_SERIES>#'"$FILEBOT_RENAME_SERIES"'#' \
        		    -e 's#<FILEBOT_RENAME_ANIMES>#'"$FILEBOT_RENAME_ANIMES"'#' -i /home/$SEEDUSER/docker/flood/filebot/postdl

			chmod +x /home/$SEEDUSER/docker/flood/filebot/postdl
			chmod +x /home/$SEEDUSER/docker/flood/filebot/postrm

			sed -i -e "s/Movies/${FILMS}/g" /home/$SEEDUSER/docker/flood/filebot/postdl
			sed -i -e "s/TV/${SERIES}/g" /home/$SEEDUSER/docker/flood/filebot/postdl
			sed -i -e "s/Music/${MUSIC}/g" /home/$SEEDUSER/docker/flood/filebot/postdl
			sed -i -e "s/Animes/${ANIMES}/g" /home/$SEEDUSER/docker/flood/filebot/postdl

			docker exec -i flood-$SEEDUSER chown -R abc:abc filebot
			
			grep -R "plex" "$INSTALLEDFILE" > /dev/null 2>&1
			if [[ "$?" == "0" ]]; then
				docker exec -t flood-$SEEDUSER sed -i 's/\<unsorted=y\>/& "exec=\/scripts\/plex_autoscan\/plex_autoscan_flood.sh"/' /usr/local/bin/postdl
			fi
			checking_errors $?
			echo ""
		fi
done
}