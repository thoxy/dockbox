#!/bin/bash

function install_fail2ban() {
		echo -e "${BLUE}### FAIL2BAN ###${NC}"
		if (whiptail --title "Docker fail2ban" --yesno "Voulez vous installer fail2ban" 7 50) then
			echo -e " ${BWHITE}* Installation de Fail2ban !${NC}"
			apt install fail2ban -y > /dev/null 2>&1
			SSH=$(echo ${SSH_CLIENT##* })
			IP_DOM=$(grep 'Accepted' /var/log/auth.log | cut -d ' ' -f12 | head -1)
			cp "$BASEDIR/includes/config/fail2ban/custom.conf" "/etc/fail2ban/jail.d/custom.conf" > /dev/null 2>&1
			cp "$BASEDIR/includes/config/fail2ban/traefik.conf" "/etc/fail2ban/jail.d/traefik.conf" > /dev/null 2>&1
			cp "$BASEDIR/includes/config/fail2ban/traefik-auth.conf" "/etc/fail2ban/filter.d/traefik-auth.conf" > /dev/null 2>&1
			cp "$BASEDIR/includes/config/fail2ban/traefik-botsearch.conf" "/etc/fail2ban/filter.d/traefik-botsearch.conf" > /dev/null 2>&1
			cp "$BASEDIR/includes/config/fail2ban/docker-action.conf" "/etc/fail2ban/action.d/docker-action.conf" > /dev/null 2>&1
			sed -i "s|%SSH%|$SSH|g" /etc/fail2ban/jail.d/custom.conf
			sed -i "s|%IP_DOM%|$IP_DOM|g" /etc/fail2ban/jail.d/custom.conf
			sed -i "s|%IP_DOM%|$IP_DOM|g" /etc/fail2ban/jail.d/traefik.conf
			cd $CONFDIR/docker/traefik
			docker-compose restart traefik > /dev/null 2>&1
			systemctl restart fail2ban.service > /dev/null 2>&1
			checking_errors $?
		else
			echo -e " ${BWHITE}* Fail2ban non installé !${NC}"	
		fi
		echo ""
}	

function install_traefik() {
	echo -e "${BLUE}### TRAEFIK ###${NC}"
	TRAEFIK="$CONFDIR/docker/traefik/"
	TRAEFIKCOMPOSEFILE="$TRAEFIK/docker-compose.yml"
	TRAEFIKTOML="$TRAEFIK/traefik.toml"
	INSTALLEDFILE="$CONFDIR/resume"
	if [[ ! -f "$INSTALLEDFILE" ]]; then
	touch $INSTALLEDFILE> /dev/null 2>&1
	fi

	if [[ ! -d "$TRAEFIK" ]]; then
		echo -e " ${BWHITE}* Installation Traefik${NC}"
		mkdir -p $TRAEFIK
		touch $TRAEFIKCOMPOSEFILE
		touch $TRAEFIKTOML
		cat "/opt/seedbox-compose/includes/dockerapps/traefik.yml" >> $TRAEFIKCOMPOSEFILE
		cat "/opt/seedbox-compose/includes/dockerapps/traefik.toml" >> $TRAEFIKTOML
		sed -i "s|%DOMAIN%|$DOMAIN|g" $TRAEFIKCOMPOSEFILE
		sed -i "s|%TRAEFIK%|$TRAEFIK|g" $TRAEFIKCOMPOSEFILE
		sed -i "s|%EMAIL%|$CONTACTEMAIL|g" $TRAEFIKTOML
		sed -i "s|%DOMAIN%|$DOMAIN|g" $TRAEFIKTOML
		sed -i "s|%VAR%|$VAR|g" $TRAEFIKCOMPOSEFILE
		cd $TRAEFIK
		docker network create traefik_proxy > /dev/null 2>&1
		docker-compose up -d > /dev/null 2>&1
		echo "traefik-port-traefik.$DOMAIN" >> $INSTALLEDFILE
		checking_errors $?
	else
		echo -e " ${YELLOW}* Traefik est déjà installé !${NC}"
	fi
	echo ""
}

function install_portainer() {
	echo -e "${BLUE}### PORTAINER ###${NC}"
	INSTALLEDFILE="$CONFDIR/resume"
	if [[ ! -f "$INSTALLEDFILE" ]]; then
	touch $INSTALLEDFILE> /dev/null 2>&1
	fi
	PORTAINER="$CONFDIR/docker/portainer/"
	PORTAINERCOMPOSEFILE="$PORTAINER/docker-compose.yml"

	if [[ ! -d "$PORTAINER" ]]; then
		mkdir -p $PORTAINER
		touch $PORTAINERCOMPOSEFILE

		if (whiptail --title "Docker Portainer" --yesno "Voulez vous installer portainer" 7 50) then
			echo -e " ${BWHITE}* Installation de portainer !${NC}"
			cat /opt/seedbox-compose/includes/dockerapps/head.docker > $PORTAINERCOMPOSEFILE
			cat "/opt/seedbox-compose/includes/dockerapps/portainer.yml" >> $PORTAINERCOMPOSEFILE
			cat /opt/seedbox-compose/includes/dockerapps/foot.docker >> $PORTAINERCOMPOSEFILE
			sed -i "s|%DOMAIN%|$DOMAIN|g" $PORTAINERCOMPOSEFILE
			sed -i "s|%PORTAINER%|$PORTAINER|g" $PORTAINERCOMPOSEFILE
			cd $PORTAINER
			docker-compose up -d > /dev/null 2>&1
			echo "portainer-port-portainer.$DOMAIN" >> $INSTALLEDFILE
			checking_errors $?
		else
			echo -e " ${BWHITE}--> portainer n'est pas installé !${NC}"
		fi
	else
		echo -e " ${BWHITE}--> portainer est déjà installé !${NC}"
	fi
	echo ""
}

function install_watchtower() {
	echo -e "${BLUE}### WATCHTOWER ###${NC}"
	INSTALLEDFILE="$CONFDIR/resume"
	if [[ ! -f "$INSTALLEDFILE" ]]; then
	touch $INSTALLEDFILE> /dev/null 2>&1
	fi
	WATCHTOWER="$CONFDIR/docker/watchtower/"
	WATCHTOWERCOMPOSEFILE="$WATCHTOWER/docker-compose.yml"

	if [[ ! -d "$WATCHTOWER" ]]; then
		mkdir -p $WATCHTOWER
		touch $WATCHTOWERCOMPOSEFILE

		if (whiptail --title "Docker watchtower" --yesno "Voulez vous installer watchtower" 7 50) then
			echo -e " ${BWHITE}* Installation de watchtower !${NC}"
			cat /opt/seedbox-compose/includes/dockerapps/head.docker > $WATCHTOWERCOMPOSEFILE
			cat "/opt/seedbox-compose/includes/dockerapps/watchtower.yml" >> $WATCHTOWERCOMPOSEFILE
			cat /opt/seedbox-compose/includes/dockerapps/foot.docker >> $WATCHTOWERCOMPOSEFILE
			sed -i "s|%DOMAIN%|$DOMAIN|g" $WATCHTOWERCOMPOSEFILE
			sed -i "s|%PORTAINER%|$PORTAINER|g" $WATCHTOWERCOMPOSEFILE
			cd $WATCHTOWER
			docker-compose up -d > /dev/null 2>&1
			checking_errors $?
		else
			echo -e " ${BWHITE}--> watchtower n'est pas installé !${NC}"
		fi
	else
		echo -e " ${BWHITE}--> watchtower est déjà installé !${NC}"
	fi
	echo ""
}

function install_plexdrive() {
	echo -e "${BLUE}### PLEXDRIVE ###${NC}"
	mkdir -p /mnt/plexdrive > /dev/null 2>&1
	PLEXDRIVE="/usr/bin/plexdrive"

	if [[ ! -e "$PLEXDRIVE" ]]; then
		echo -e " ${BWHITE}* Installation plexdrive${NC}"
		cd /tmp
		wget $(curl -s https://api.github.com/repos/dweidenfeld/plexdrive/releases/latest | grep 'browser_' | cut -d\" -f4 | grep plexdrive-linux-amd64) -q -O plexdrive > /dev/null 2>&1
		chmod -c +x /tmp/plexdrive > /dev/null 2>&1
		#install plexdrive
		mv -v /tmp/plexdrive /usr/bin/ > /dev/null 2>&1
		chown -c root:root /usr/bin/plexdrive > /dev/null 2>&1
		echo ""
		echo -e " ${YELLOW}* Dès que le message ${NC}${CPURPLE}"First cache build process finished!"${NC}${YELLOW} apparait à l'écran, taper ${NC}${CPURPLE}CTRL + C${NC}${YELLOW} pour poursuivre le script !${NC}"
		echo ""
		/usr/bin/plexdrive mount -v 3 /mnt/plexdrive
		cp "$BASEDIR/includes/config/systemd/plexdrive.service" "/etc/systemd/system/plexdrive.service" > /dev/null 2>&1
		systemctl daemon-reload > /dev/null 2>&1
		systemctl enable plexdrive.service > /dev/null 2>&1
		systemctl start plexdrive.service > /dev/null 2>&1
		echo ""
		echo -e " ${GREEN}* Configuration Plexdrive terminée avec succés !${NC}"
	else
		echo -e " ${YELLOW}* Plexdrive est déjà installé !${NC}"
	fi
	echo ""
}

function install_rclone() {
	echo -e "${BLUE}### RCLONE ###${NC}"
	mkdir /mnt/rclone > /dev/null 2>&1
	RCLONECONF="/root/.config/rclone/rclone.conf"
	USERID=$(id -u $SEEDUSER)
	GRPID=$(id -g $SEEDUSER)

	if [[ ! -f "$RCLONECONF" ]]; then
		echo -e " ${BWHITE}* Installation rclone${NC}"
		clear
		rclone_aide
		pause
		curl https://rclone.org/install.sh | bash > /dev/null 2>&1
		mkdir -p /root/.config/rclone
		echo ""
    		echo -e "${YELLOW}\nColler le contenu de rclone.conf avec le clic droit, appuyer ensuite sur la touche Entrée et Taper ${CPURPLE}STOP${CEND}${YELLOW} pour poursuivre le script.\n${NC}"   				
		while :
    		do		
        	read -p "" EXCLUDEPATH
        		if [[ "$EXCLUDEPATH" = "STOP" ]] || [[ "$EXCLUDEPATH" = "stop" ]]; then
            				break
        		fi
        	echo "$EXCLUDEPATH" >> /root/.config/rclone/rclone.conf
    		done
		echo ""
		REMOTE=$(grep -iC 2 "token" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
		REMOTEPLEX=$(grep -iC 2 "/mnt/plexdrive" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
		REMOTECRYPT=$(grep -v -e $REMOTEPLEX -e $REMOTE /root/.config/rclone/rclone.conf | grep "\[" | sed "s/\[//g" | sed "s/\]//g")
		clear
		echo -e " ${BWHITE}* Remote chiffré rclone${NC} --> ${YELLOW}$REMOTECRYPT:${NC}"
		checking_errors $?
		echo ""
		echo -e " ${BWHITE}* Remote chiffré plexdrive${NC} --> ${YELLOW}$REMOTEPLEX:${NC}"
		checking_errors $?
		echo ""

		mkdir -p /mnt/rclone/$SEEDUSER

		cp "$BASEDIR/includes/config/systemd/rclone.service" "/etc/systemd/system/rclone-$SEEDUSER.service" > /dev/null 2>&1
		sed -i "s|%REMOTEPLEX%|$REMOTEPLEX:|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%SEEDUSER%|$SEEDUSER|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%USERID%|$USERID|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%GRPID%|$GRPID|g" /etc/systemd/system/rclone-$SEEDUSER.service

		systemctl daemon-reload > /dev/null 2>&1
		systemctl enable rclone-$SEEDUSER.service > /dev/null 2>&1
		service rclone-$SEEDUSER start
		var="Montage rclone en cours, merci de patienter..."
		decompte 15
		checking_errors $?
	fi
	echo ""
}

function install_docker() {
	echo -e "${BLUE}### DOCKER ###${NC}"
	dpkg-query -l docker > /dev/null 2>&1
  	if [ $? != 0 ]; then
		echo " * Installation Docker"
		curl -fsSL https://get.docker.com -o get-docker.sh
		sh get-docker.sh
		if [[ "$?" == "0" ]]; then
			echo -e "	${GREEN}* Installation Docker réussie${NC}"
		else
			echo -e "	${RED}* Echec de l'installation Docker !${NC}"
		fi
		service docker start > /dev/null 2>&1
		echo " * Installing Docker-compose"
		curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
		chmod +x /usr/local/bin/docker-compose
		if [[ "$?" == "0" ]]; then
			echo -e "	${GREEN}* Installation Docker-Compose réussie${NC}"
		else
			echo -e "	${RED}* Echec de l'installation Docker-Compose !${NC}"
		fi
		echo ""
	else
		echo -e " ${YELLOW}* Docker est déjà installé !${NC}"
		echo ""
	fi
}

function install_cloudplow() {
	echo -e "${BLUE}### CLOUDPLOW ###${NC}"
	echo -e " ${BWHITE}* Installation cloudplow${NC}"
	CLOUDPLOWFOLDER="/home/$SEEDUSER/scripts"
	if [[ ! -d $CLOUDPLOWFOLDER ]]; then
	mkdir -p $CLOUDPLOWFOLDER
	fi
	cd $CLOUDPLOWFOLDER
	git clone https://github.com/l3uddz/cloudplow > /dev/null 2>&1
	cd $CLOUDPLOWFOLDER/cloudplow
	python3 -m pip install -r requirements.txt > /dev/null 2>&1

	CLOUDPLOWFILE="$CLOUDPLOWFOLDER/cloudplow/config.json.sample"
	if [[ -e "$CLOUDPLOWFILE" ]]; then
	mv $CLOUDPLOWFILE $CLOUDPLOWFOLDER/cloudplow/config.json
	fi

	## récupération des variables
	SEEDGROUP=$(cat $GROUPFILE)
	grep -R "plex" "$INSTALLEDFILE" > /dev/null 2>&1
	if [[ "$?" == "0" ]]; then
	docker exec -ti plex-$SEEDUSER grep -E -o "PlexOnlineToken=.{0,22}" /config/Library/Application\ Support/Plex\ Media\ Server/Preferences.xml > /home/$SEEDUSER/token.txt
	TOKEN=$(grep PlexOnlineToken /home/$SEEDUSER/token.txt | cut -d '=' -f2 | cut -c2-21)
	fi

	REMOTE=$(grep -iC 2 "token" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
	REMOTEPLEX=$(grep -iC 2 "/mnt/plexdrive" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
	REMOTECRYPT=$(grep -v -e $REMOTEPLEX -e $REMOTE /root/.config/rclone/rclone.conf | grep "\[" | sed "s/\[//g" | sed "s/\]//g")

	## intégration des variables dans config.json
	CLOUDPLOW="/home/$SEEDUSER/scripts/cloudplow/config.json"
	cat "$BASEDIR/includes/config/cloudplow/config.json" > $CLOUDPLOW
	sed -i "s|%SEEDUSER%|$SEEDUSER|g" $CLOUDPLOW
	sed -i "s|%SEEDGROUP%|$SEEDGROUP|g" $CLOUDPLOW
	grep -R "plex" "$INSTALLEDFILE" > /dev/null 2>&1
	if [[ "$?" == "0" ]]; then
		sed -i "s|%TOKEN%|$TOKEN|g" $CLOUDPLOW
	fi
	sed -i "s|%ACCESSDOMAIN%|$ACCESSDOMAIN|g" $CLOUDPLOW
	sed -i "s|%REMOTECRYPT%|$REMOTECRYPT|g" $CLOUDPLOW

	## configuration cloudplow.service
	cp "$BASEDIR/includes/config/systemd/cloudplow.service" "/etc/systemd/system/cloudplow-$SEEDUSER.service" > /dev/null 2>&1
	sed -i "s|%SEEDUSER%|$SEEDUSER|g" /etc/systemd/system/cloudplow-$SEEDUSER.service
	sed -i "s|%SEEDGROUP%|$SEEDGROUP|g" /etc/systemd/system/cloudplow-$SEEDUSER.service
	systemctl daemon-reload > /dev/null 2>&1
	systemctl enable cloudplow-$SEEDUSER.service > /dev/null 2>&1
	systemctl start cloudplow-$SEEDUSER.service > /dev/null 2>&1
	checking_errors $?
	grep -R "plex" "$INSTALLEDFILE" > /dev/null 2>&1
	if [[ "$?" == "0" ]]; then
	rm /home/$SEEDUSER/token.txt
	fi
	echo ""
}

function install_plex_autoscan() {
	echo -e "${BLUE}### PLEX_AUTOSCAN ###${NC}"
	echo -e " ${BWHITE}* Installation plex_autoscan${NC}"

	## install plex_autoscan
	SEEDGROUP=$(cat $GROUPFILE)
	git clone https://github.com/l3uddz/plex_autoscan /home/$SEEDUSER/scripts/plex_autoscan > /dev/null 2>&1
	chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/scripts/plex_autoscan
	cd /home/$SEEDUSER/scripts/plex_autoscan
	python -m pip install -r requirements.txt > /dev/null 2>&1

	## configuration plex_autoscan.service
	cp "$BASEDIR/includes/config/systemd/plex_autoscan.service" "/etc/systemd/system/plex_autoscan-$SEEDUSER.service" > /dev/null 2>&1
	sed -i "s|%SEEDUSER%|$SEEDUSER|g" /etc/systemd/system/plex_autoscan-$SEEDUSER.service
	sed -i "s|%SEEDGROUP%|$SEEDGROUP|g" /etc/systemd/system/plex_autoscan-$SEEDUSER.service
	systemctl daemon-reload > /dev/null 2>&1
	systemctl enable plex_autoscan-$SEEDUSER.service > /dev/null 2>&1
	checking_errors $?
	echo ""
}

function install_plex_dupefinder() {
	echo -e "${BLUE}### PLEX_DUPEFINDER ###${NC}"
	echo -e " ${BWHITE}* Installation plex_dupefinder${NC}"

	## install plex_dupefinder
	SEEDGROUP=$(cat $GROUPFILE)
	git clone https://github.com/l3uddz/plex_dupefinder /home/$SEEDUSER/scripts/plex_dupefinder > /dev/null 2>&1
	chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/scripts/plex_dupefinder
	cd /home/$SEEDUSER/scripts/plex_dupefinder
	python3 -m pip install -r requirements.txt > /dev/null 2>&1
	python3 plexdupes.py > /dev/null 2>&1
}

function install_services() {
	replace_media_compose
	USERID=$(id -u $SEEDUSER)
	GRPID=$(id -g $SEEDUSER)
	INSTALLEDFILE="/home/$SEEDUSER/resume"
	touch $INSTALLEDFILE > /dev/null 2>&1

	## port rtorrent
	if [[ -f "$FILEPORTPATH" ]]; then
		declare -i PORT=$(cat $FILEPORTPATH | tail -1)
	else
		declare -i PORT=$FIRSTPORT
	fi

	## port plex
	if [[ -f "$PLEXPORTPATH" ]]; then
		declare -i PORTPLEX=$(cat $PLEXPORTPATH | tail -1)
	else
		declare -i PORTPLEX=32400
	fi

	## préparation du docker-compose
	DOCKERCOMPOSEFILE="/home/$SEEDUSER/docker-compose.yml"
	if [[ ! -f "$DOCKERCOMPOSEFILE" ]]; then
	cat /opt/seedbox-compose/includes/dockerapps/head.docker > $DOCKERCOMPOSEFILE
	cat /opt/seedbox-compose/includes/dockerapps/foot.docker >> $DOCKERCOMPOSEFILE
	fi
	for line in $(cat $SERVICESPERUSER);
	do
		sed -i -n -e :a -e '1,5!{P;N;D;};N;ba' $DOCKERCOMPOSEFILE
		cat "/opt/seedbox-compose/includes/dockerapps/$line.yml" >> $DOCKERCOMPOSEFILE
		sed -i "s|%TIMEZONE%|$TIMEZONE|g" $DOCKERCOMPOSEFILE
		sed -i "s|%UID%|$USERID|g" $DOCKERCOMPOSEFILE
		sed -i "s|%GID%|$GRPID|g" $DOCKERCOMPOSEFILE
		sed -i "s|%PORT%|$PORT|g" $DOCKERCOMPOSEFILE
		sed -i "s|%PORTPLEX%|$PORTPLEX|g" $DOCKERCOMPOSEFILE
		sed -i "s|%VAR%|$VAR|g" $DOCKERCOMPOSEFILE
		sed -i "s|%DOMAIN%|$DOMAIN|g" $DOCKERCOMPOSEFILE
		sed -i "s|%USER%|$SEEDUSER|g" $DOCKERCOMPOSEFILE
		sed -i "s|%EMAIL%|$CONTACTEMAIL|g" $DOCKERCOMPOSEFILE
		sed -i "s|%FILMS%|$FILMS|g" $DOCKERCOMPOSEFILE
		sed -i "s|%SERIES%|$SERIES|g" $DOCKERCOMPOSEFILE
		sed -i "s|%ANIMES%|$ANIMES|g" $DOCKERCOMPOSEFILE
		sed -i "s|%MUSIC%|$MUSIC|g" $DOCKERCOMPOSEFILE
		cat /opt/seedbox-compose/includes/dockerapps/foot.docker >> $DOCKERCOMPOSEFILE
		NOMBRE=$(sed -n "/$SEEDUSER/=" $CONFDIR/users)
		if [ $NOMBRE -le 1 ] ; then
			FQDNTMP="$line.$DOMAIN"
		else
			FQDNTMP="$line-$SEEDUSER.$DOMAIN"
		fi
		FQDN=$(whiptail --title "SSL Subdomain" --inputbox \
		"Souhaitez vous utiliser un autre Sous Domaine pour $line ? default :" 7 75 "$FQDNTMP" 3>&1 1>&2 2>&3)
		ACCESSURL=$FQDN
		TRAEFIKURL=(Host:$ACCESSURL)
		sed -i "s|%TRAEFIKURL%|$TRAEFIKURL|g" /home/$SEEDUSER/docker-compose.yml
		sed -i "s|%ACCESSURL%|$ACCESSURL|g" $DOCKERCOMPOSEFILE
		check_domain $ACCESSURL
		echo "$line-$PORT-$FQDN" >> $INSTALLEDFILE
		URI="/"
	
		PORT=$PORT+1
		PORTPLEX=$PORTPLEX+1
		FQDN=""
		FQDNTMP=""
	done
	echo $PORT >> $FILEPORTPATH
	echo $PORTPLEX >> $PLEXPORTPATH
	echo ""
}