## PARAMETERS

CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"
CPURPLE="${CSI}1;35m"
CCYAN="${CSI}1;36m"
RED='\e[0;31m'
GREEN='\033[0;32m'
BLUEDARK='\033[0;34m'
BLUE='\e[0;36m'
YELLOW='\e[0;33m'
BWHITE='\e[1;37m'
NC='\033[0m'
DATE=`date +%d/%m/%Y-%H:%M:%S`
IPADDRESS=$(hostname -I | cut -d\  -f1)
FIRSTPORT="45002"
LASTPORT="8080"
CURRENTDIR="$PWD"
BASEDIR="/opt/dockbox"
CONFDIR="/opt/dockbox/config"
SERVICESAVAILABLE="$BASEDIR/includes/config/services-available"
MEDIAVAILABLE="$BASEDIR/includes/config/media-available"
SERVICES="$BASEDIR/includes/config/services"
SERVICESUSER="/opt/dockbox/config/services-"
MEDIASUSER="/opt/dockbox/media-"
FILEPORTPATH="/opt/dockbox/config/ports.pt"
SCANPORTPATH="/opt/dockbox/config/scan.pt"
PLEXPORTPATH="/opt/dockbox/config/plex.pt"
PACKAGESFILE="$BASEDIR/includes/config/packages"
USERSFILE="/opt/dockbox/config/users"
GROUPFILE="/opt/dockbox/config/group"

export NEWT_COLORS='
  window=,white
  border=green,blue
  textbox=black,white
'
