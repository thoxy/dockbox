#!/bin/bash
##########
function logo() {

source includes/variables.sh
source includes/variables.sh
source includes/special-install.sh
source includes/config_post_compose.sh


color1='\033[1;31m'    # light red
color2='\033[1;35m'    # light purple
color3='\033[0;33m'    # light yellow
color4='\033[1;36m'    # cyan
color5='\033[34m'      # blue
color6='\033[90m'      # gray
color7='\033[92m'      # green
nocolor='\033[39m'      # no color
companyname='\033[1;34mDockBox\033[0m'
divisionname='\033[1;36mThoxy\033[0m - Based on source from \033[1;32mlaster13\033[0m'

list1='\033[1;31mTraefik + LetsEncrypt - Watchtower - Portainer - Netdata\033[0m'
list2='\033[0;33mMedia server and Seedbox\033[0m'
list3='\033[92mTorrent, Newsgroup, Media Server,\033[0m'
list4='\033[92mDirect Download, Streaming and more allowed\033[0m'


printf "\n                ${color6}##        ${color5}.       ${companyname}${nocolor}\n"
printf "          ${color6}## ## ##       ${color5}==       ${nocolor}\n"
printf "       ${color6}## ## ## ##      ${color5}===       ${divisionname}${nocolor}\n"
printf "   ${color5}/\"\"\"\"\"\"\"\"\"\"\"\"\"\"\\___/ ===       ${color3}`lsb_release -s -d`${nocolor}\n"
printf "  ${color5}{                      /  ===-  ${nocolor}`uname -srmo`${nocolor}\n"
printf "   ${color5}\\______ ${nocolor}o${color5}          __/         ${nocolor}`date +"%A, %e %B %Y, %r"`${nocolor}\n"
printf "     \\    ${color5}\\        __/            ${color2}Seedbox docker${nocolor}\n"
printf "      \\____${color5}\\______/               ${nocolor}${list1}${nocolor}\n"
printf "        ${color4}||      ${color1}/\\                ${nocolor}${list2}${nocolor}\n"
printf "        ${color4}||     ${color1}/  \\               ${nocolor}${list3}${nocolor}\n"
printf "       ${color4}\\  /     ${color1}||                ${nocolor}${list4}\n"
printf "        ${color4}\\/      ${color1}||                ${nocolor}Uptime: `/usr/bin/uptime -p`\n"
}

function check_domain() {
		TESTDOMAIN=$1
		echo -e " ${BWHITE}* Checking domain - ping $TESTDOMAIN...${NC}"
		ping -c 1 $TESTDOMAIN | grep "$IPADDRESS" > /dev/null
		checking_errors $?
}

function rclone_aide() {
echo ""
echo -e "${CCYAN}### MODELE RCLONE.CONF ###${NC}"
echo ""
echo -e "${YELLOW}[remote non chiffré]${NC}"
echo -e "${BWHITE}type = drive${NC}"
echo -e "${BWHITE}token = {"access_token":"xxxxxxxxxxxxxxxxxx"}${NC}"
echo ""
echo -e "${YELLOW}[remote_chiffré_plexdrive]${NC}"
echo -e "${BWHITE}type = crypt${NC}"
echo -e "${BWHITE}remote = ${NC}${YELLOW}/mnt/plexdrive/Medias${NC}"
echo -e "${BWHITE}filename_encryption = standard${NC}"
echo -e "${BWHITE}password = -xxxxxxxxxxxxxxxxxx${NC}"
echo -e "${BWHITE}password2 = xxxxxxxxxxxxxxxxxx${NC}"
echo ""
echo -e "${YELLOW}[remote_chiffré_rclone]${NC}"
echo -e "${BWHITE}type = crypt${NC}"
echo -e "${BWHITE}remote = ${NC}${YELLOW}<remote non chiffré>:Medias${NC}"
echo -e "${BWHITE}filename_encryption = standard${NC}"
echo -e "${BWHITE}password = xxxxxxxxxxxxxxxxxx${NC}"
echo -e "${BWHITE}password2 = xxxxxxxxxxxxxxxxxx${NC}"
echo ""
}

function check_dir() {
	if [[ $1 != $BASEDIR ]]; then
		cd $BASEDIR
	fi
}

function script_classique() {
	if [[ -d "$CONFDIR" ]]; then
	clear
	logo
	echo ""
	echo -e "${CCYAN}SEEDBOX CLASSIQUE${CEND}"
	echo -e "${CGREEN}${CEND}"
	echo -e "${CGREEN}   1) Desinstaller la seedbox ${CEND}"
	echo -e "${CGREEN}   2) Ajout/Supression d'utilisateurs${CEND}"
	echo -e "${CGREEN}   3) Ajout/Supression d'Applis${CEND}"

	echo -e ""
	read -p "Votre choix [1-3]: " -e -i 1 PORT_CHOICE

	case $PORT_CHOICE in
		1) ## Installation de la seedbox
		clear
		echo ""
		echo -e "${YELLOW}### Seedbox-Compose déjà installée !###${NC}"
		if (whiptail --title "Seedbox-Compose déjà installée" --yesno "Désinstaller complètement la Seedbox ?" 7 50) then
			uninstall_seedbox
		else
			script_classique
		fi
		;;
		2)
		clear
		logo
		## Ajout d'Utilisateurs
		## Defines parameters for dockers : password, domains and replace it in docker-compose file
		clear
			manage_users
 		;;
		3)
		clear
		logo
		## Ajout d'Applications
		echo""
		clear
			manage_apps
		;;
	esac
	fi
}

function script_plexdrive() {
	if [[ -d "$CONFDIR" ]]; then
	clear
	logo
	echo ""
	echo -e "${CCYAN}SEEDBOX RCLONE/PLEXDRIVE${CEND}"
	echo -e "${CGREEN}${CEND}"
	echo -e "${CGREEN}   1) Désinstaller la seedbox ${CEND}"
	echo -e "${CGREEN}   2) Ajout/Supression d'utilisateurs${CEND}"
	echo -e "${CGREEN}   3) Ajout/Supression d'Applis${CEND}"

	echo -e ""
	read -p "Votre choix [1-3]: " -e -i 1 PORT_CHOICE

	case $PORT_CHOICE in
		1) ## Installation de la seedbox
		clear
		echo ""
		echo -e "${YELLOW}### Seedbox-Compose déjà installée !###${NC}"
		if (whiptail --title "Seedbox-Compose déjà installée" --yesno "Désinstaller complètement la Seedbox ?" 7 50) then
			uninstall_seedbox
		else
			script_plexdrive
		fi
		;;
		2)
		clear
		logo
		## Ajout d'Utilisateurs
		## Defines parameters for dockers : password, domains and replace it in docker-compose file
		clear
			manage_users
 		;;
		3)
		clear
		logo
		## Ajout d'Applications
		echo""
		clear
			manage_apps
		;;
	esac
	fi
}

function conf_dir() {
	if [[ ! -d "$CONFDIR" ]]; then
		mkdir $CONFDIR > /dev/null 2>&1
	fi
}

function install_base_packages() {
	echo ""
	echo -e "${BLUE}### INSTALLATION DES PACKAGES ###${NC}"
	whiptail --title "Base Package" --msgbox "Seedbox-Compose va maintenant installer les Pré-Requis et vérifier la mise à jour du système" 10 60
	echo -e " ${BWHITE}* Installation apache2-utils, unzip, git, curl ...${NC}"
	{
	NUMPACKAGES=$(cat $PACKAGESFILE | wc -l)
	for package in $(cat $PACKAGESFILE);
	do
		apt-get install -y $package
		echo $NUMPACKAGES
		NUMPACKAGES=$(($NUMPACKAGES+(100/$NUMPACKAGES)))
	done
	} | whiptail --gauge "Merci de patienter pendant l'installation des packages !" 6 70 0
	checking_errors $?
	echo ""
}

function checking_system() {
	echo -e "${BLUE}### VERIFICATION SYSTEME ###${NC}"
	echo -e " ${BWHITE}* Vérification du système OS${NC}"
	TMPSYSTEM=$(gawk -F= '/^NAME/{print $2}' /etc/os-release)
	TMPCODENAME=$(lsb_release -sc)
	TMPRELEASE=$(cat /etc/debian_version)
	if [[ $(echo $TMPSYSTEM | sed 's/\"//g') == "Debian GNU/Linux" ]]; then
		SYSTEMOS="Debian"
		if [[ $(echo $TMPRELEASE | grep "8") != "" ]]; then
			SYSTEMRELEASE=$TMPRELEASE
			SYSTEMCODENAME="jessie"
		elif [[ $(echo $TMPRELEASE | grep "9") != "" ]]; then
			SYSTEMRELEASE=$TMPRELEASE
			SYSTEMCODENAME="stretch"
		fi
	elif [[ $(echo $TMPSYSTEM | sed 's/\"//g') == "Ubuntu" ]]; then
		SYSTEMOS="Ubuntu"
		if [[ $(echo $TMPCODENAME | grep "xenial") != "" ]]; then
			SYSTEMRELEASE="16.04"
			SYSTEMCODENAME="xenial"
		elif [[ $(echo $TMPCODENAME | grep "yakkety") != "" ]]; then
			SYSTEMRELEASE="16.10"
			SYSTEMCODENAME="yakkety"
		elif [[ $(echo $TMPCODENAME | grep "zesty") != "" ]]; then
			SYSTEMRELEASE="17.14"
			SYSTEMCODENAME="zesty"
		elif [[ $(echo $TMPCODENAME | grep "cosmic") != "" ]]; then
			SYSTEMRELEASE="18.10"
			SYSTEMCODENAME="cosmic"
		fi
	fi
	echo -e "	${YELLOW}--> System OS : $SYSTEMOS${NC}"
	echo -e "	${YELLOW}--> Release : $SYSTEMRELEASE${NC}"
	echo -e "	${YELLOW}--> Codename : $SYSTEMCODENAME${NC}"
	echo -e " ${BWHITE}* Updating & upgrading system${NC}"
	apt-get update > /dev/null 2>&1
	apt-get upgrade -y > /dev/null 2>&1
	checking_errors $?
	echo ""
}

function checking_errors() {
	if [[ "$1" == "0" ]]; then
		echo -e "	${GREEN}--> Operation success !${NC}"
	else
		echo -e "	${RED}--> Operation failed !${NC}"
	fi
}

function rclone_service() {
		REMOTE=$(grep -iC 2 "token" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
		REMOTEPLEX=$(grep -iC 2 "/mnt/plexdrive" /root/.config/rclone/rclone.conf | head -n 1 | sed "s/\[//g" | sed "s/\]//g")
		REMOTECRYPT=$(grep -v -e $REMOTEPLEX -e $REMOTE /root/.config/rclone/rclone.conf | grep "\[" | sed "s/\[//g" | sed "s/\]//g")
		clear
		echo -e " ${BWHITE}* Remote chiffré rclone${NC} --> ${YELLOW}$REMOTECRYPT:${NC}"
		checking_errors $?
		echo ""
		echo -e " ${BWHITE}* Remote chiffré plexdrive${NC} --> ${YELLOW}$REMOTEPLEX:${NC}"
		checking_errors $?
		echo ""

		mkdir -p /mnt/rclone/$SEEDUSER

		cp "$BASEDIR/includes/config/systemd/rclone.service" "/etc/systemd/system/rclone-$SEEDUSER.service" > /dev/null 2>&1
		sed -i "s|%REMOTEPLEX%|$REMOTEPLEX:|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%SEEDUSER%|$SEEDUSER|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%USERID%|$USERID|g" /etc/systemd/system/rclone-$SEEDUSER.service
		sed -i "s|%GRPID%|$GRPID|g" /etc/systemd/system/rclone-$SEEDUSER.service

		systemctl daemon-reload > /dev/null 2>&1
		systemctl enable rclone-$SEEDUSER.service > /dev/null 2>&1
		service rclone-$SEEDUSER start
		var="Montage rclone en cours, merci de patienter..."
		decompte 15
		checking_errors $?
		echo ""
}

function unionfs_fuse() {
	echo -e "${BLUE}### Unionfs-Fuse ###${NC}"
	UNIONFS="/etc/systemd/system/unionfs-$SEEDUSER.service"
	if [[ ! -e "$UNIONFS" ]]; then
		echo -e " ${BWHITE}* Installation Unionfs${NC}"
		cp "$BASEDIR/includes/config/systemd/unionfs.service" "/etc/systemd/system/unionfs-$SEEDUSER.service" > /dev/null 2>&1
		sed -i "s|%SEEDUSER%|$SEEDUSER|g" /etc/systemd/system/unionfs-$SEEDUSER.service
		systemctl daemon-reload > /dev/null 2>&1
		systemctl enable unionfs-$SEEDUSER.service > /dev/null 2>&1
		systemctl start unionfs-$SEEDUSER.service > /dev/null 2>&1
		checking_errors $?
	else
		echo -e " ${YELLOW}* Unionfs est déjà installé pour l'utilisateur $SEEDUSER !${NC}"
		systemctl restart unionfs-$SEEDUSER.service
	fi
	echo ""
}

function define_parameters() {
	echo -e "${BLUE}### INFORMATIONS UTILISATEURS ###${NC}"
	USEDOMAIN="y"
	CURRTIMEZONE=$(cat /etc/timezone)
	create_user
	CONTACTEMAIL=$(whiptail --title "Adresse Email" --inputbox \
	"Merci de taper votre adresse Email :" 7 50 3>&1 1>&2 2>&3)
	TIMEZONEDEF=$(whiptail --title "Timezone" --inputbox \
	"Merci de vérifier votre timezone" 7 66 "$CURRTIMEZONE" \
	3>&1 1>&2 2>&3)
	if [[ $TIMEZONEDEF == "" ]]; then
		TIMEZONE=$CURRTIMEZONE
	else
		TIMEZONE=$TIMEZONEDEF
	fi
	DOMAIN=$(whiptail --title "Votre nom de Domaine" --inputbox \
	"Merci de taper votre nom de Domaine :" 7 50 3>&1 1>&2 2>&3)
	echo ""
}

function create_user() {
	if [[ ! -f "$GROUPFILE" ]]; then
		touch $GROUPFILE
		SEEDGROUP=$(whiptail --title "Group" --inputbox \
        	"Création d'un groupe pour la Seedbox" 7 50 3>&1 1>&2 2>&3)
		echo "$SEEDGROUP" > "$GROUPFILE"
    		egrep "^$SEEDGROUP" /etc/group >/dev/null
		if [[ "$?" != "0" ]]; then
			echo -e " ${BWHITE}* Création du groupe $SEEDGROUP"
	    	groupadd $SEEDGROUP
	    	checking_errors $?
		else
			SEEDGROUP=$TMPGROUP
	    	echo -e " ${YELLOW}* Le groupe $SEEDGROUP existe déjà.${NC}"
		fi
		if [[ ! -f "$USERSFILE" ]]; then
			touch $USERSFILE
		fi
		SEEDUSER=$(whiptail --title "Administrateur" --inputbox \
			"Nom d'Administrateur de la Seedbox :" 7 50 3>&1 1>&2 2>&3)
		PASSWORD=$(whiptail --title "Password" --passwordbox \
			"Mot de passe :" 7 50 3>&1 1>&2 2>&3)
		egrep "^$SEEDUSER" /etc/passwd >/dev/null
		if [ $? -eq 0 ]; then
			echo -e " ${YELLOW}* L'utilisateur existe déjà !${NC}"
			USERID=$(id -u $SEEDUSER)
			GRPID=$(id -g $SEEDUSER)
			echo -e " ${BWHITE}* Ajout de $SEEDUSER in $SEEDGROUP"
			usermod -a -G $SEEDGROUP $SEEDUSER
			checking_errors $?
		else
			PASS=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD)
			echo -e " ${BWHITE}* Ajout de $SEEDUSER au système"
			useradd -M -g $SEEDGROUP -p $PASS -s /bin/bash $SEEDUSER > /dev/null 2>&1
			mkdir -p /home/$SEEDUSER
			chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER
			chmod 755 /home/$SEEDUSER
			checking_errors $?
			USERID=$(id -u $SEEDUSER)
			GRPID=$(id -g $SEEDUSER)
		fi
		add_user_htpasswd $SEEDUSER $PASSWORD
		echo $SEEDUSER >> $USERSFILE
		return
	else
		TMPGROUP=$(cat $GROUPFILE)
		if [[ "$TMPGROUP" == "" ]]; then
			SEEDGROUP=$(whiptail --title "Group" --inputbox \
        		"Création d'un groupe pour la Seedbox" 7 50 3>&1 1>&2 2>&3)
        	fi
	fi
    	egrep "^$SEEDGROUP" /etc/group >/dev/null
	if [[ "$?" != "0" ]]; then
		echo -e " ${BWHITE}* Création du groupe $SEEDGROUP"
	    groupadd $SEEDGROUP
	    checking_errors $?
	else
		SEEDGROUP=$TMPGROUP
	    echo -e " ${YELLOW}* Le groupe $SEEDGROUP existe déjà.${NC}"
	fi
	if [[ ! -f "$USERSFILE" ]]; then
		touch $USERSFILE
	fi
	SEEDUSER=$(whiptail --title "Utilisateur" --inputbox \
		"Nom d'utilisateur :" 7 50 3>&1 1>&2 2>&3)
	PASSWORD=$(whiptail --title "Password" --passwordbox \
		"Mot de passe :" 7 50 3>&1 1>&2 2>&3)
	egrep "^$SEEDUSER" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo -e " ${YELLOW}* L'utilisateur existe déjà !${NC}"
		USERID=$(id -u $SEEDUSER)
		GRPID=$(id -g $SEEDUSER)
		echo -e " ${BWHITE}* Ajout de $SEEDUSER in $SEEDGROUP"
		usermod -a -G $SEEDGROUP $SEEDUSER
		checking_errors $?
	else
		PASS=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD)
		echo -e " ${BWHITE}* Ajout de $SEEDUSER au système"
		useradd -M -g $SEEDGROUP -p $PASS -s /bin/bash $SEEDUSER > /dev/null 2>&1
		mkdir -p /home/$SEEDUSER
		chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER
		chmod 755 /home/$SEEDUSER
		checking_errors $?
		USERID=$(id -u $SEEDUSER)
		GRPID=$(id -g $SEEDUSER)
	fi
	add_user_htpasswd $SEEDUSER $PASSWORD
	echo $SEEDUSER >> $USERSFILE
}


function add_ftp () {
	docker exec -i ftp /bin/bash << EOX
	( echo ${PASSWORD} ; echo ${PASSWORD} )|pure-pw useradd ${SEEDUSER} -f /etc/pure-ftpd/passwd/pureftpd.passwd -m -u ftpuser -d /home/ftpusers/${SEEDUSER}
EOX
}

function del_ftp () {
	docker exec -i ftp /bin/bash << EOC
    pure-pw userdel ${SEEDUSER} -f /etc/pure-ftpd/passwd/pureftpd.passwd
EOC
}

function choose_services() {
	echo -e "${BLUE}### SERVICES ###${NC}"
	echo -e " ${BWHITE}--> Services en cours d'installation : ${NC}"
	for app in $(cat $SERVICESAVAILABLE);
	do
		service=$(echo $app | cut -d\- -f1)
		desc=$(echo $app | cut -d\- -f2)
		echo "$service $desc off" >> /tmp/menuservices.txt
	done
	SERVICESTOINSTALL=$(whiptail --title "Gestion des Applications" --checklist \
	"Applis à ajouter pour $SEEDUSER (Barre espace pour la sélection)" 28 60 17 \
	$(cat /tmp/menuservices.txt) 3>&1 1>&2 2>&3)
	SERVICESPERUSER="$SERVICESUSER$SEEDUSER"
	touch $SERVICESPERUSER
	for APPDOCKER in $SERVICESTOINSTALL
	do
		echo -e "	${GREEN}* $(echo $APPDOCKER | tr -d '"')${NC}"
		echo $(echo ${APPDOCKER,,} | tr -d '"') >> $SERVICESPERUSER
	done
	if [[ "$DOMAIN" == "" ]]; then
		DOMAIN=$(whiptail --title "Votre nom de Domaine" --inputbox \
		"Merci de taper votre nom de Domaine :" 7 50 3>&1 1>&2 2>&3)
	fi

	rm /tmp/menuservices.txt
}

function choose_media_folder_classique() {
	echo -e "${BLUE}### DOSSIERS MEDIAS ###${NC}"
	echo -e " ${BWHITE}--> Création des dossiers Medias : ${NC}"
	for media in $(cat $MEDIAVAILABLE);
	do
		service=$(echo $media | cut -d\- -f1)
		desc=$(echo $media | cut -d\- -f2)
		echo "$service $desc off" >> /tmp/menumedia.txt
	done
	MEDIASTOINSTALL=$(whiptail --title "Gestion des dossiers Medias" --checklist \
	"Medias à ajouter pour $SEEDUSER (Barre espace pour la sélection)" 28 60 17 \
	$(cat /tmp/menumedia.txt) 3>&1 1>&2 2>&3)
	MEDIASPERUSER="$MEDIASUSER$SEEDUSER"
	touch $MEDIASPERUSER
	for MEDDOCKER in $MEDIASTOINSTALL
	do
		echo -e "	${GREEN}* $(echo $MEDDOCKER | tr -d '"')${NC}"
		echo $(echo ${MEDDOCKER} | tr -d '"') >> $MEDIASPERUSER
	done
	for line in $(cat $MEDIASPERUSER);
	do
	line=$(echo $line | sed 's/\(.\)/\U\1/')
	mkdir -p /home/$SEEDUSER/Medias/$line
	chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/Medias
	done
	rm /tmp/menumedia.txt
	echo ""
}

function choose_media_folder_plexdrive() {
	echo -e "${BLUE}### DOSSIERS MEDIAS ###${NC}"
	FOLDER="/mnt/rclone/$SEEDUSER"
	MEDIASPERUSER="$MEDIASUSER$SEEDUSER"
	
	# si le dossier /mnt/rclone/user n'est pas vide
	if [ "$(ls -A /mnt/rclone/$SEEDUSER)" ]; then
		cd /mnt/rclone/$SEEDUSER
		ls -Ad */ | sed 's,/$,,g' > $MEDIASPERUSER
		mkdir -p /home/$SEEDUSER/Medias
		echo -e " ${BWHITE}--> Récupération des dossiers Utilisateur à partir de Gdrive... : ${NC}"
		for line in $(cat $MEDIASPERUSER);
		do
		mkdir -p /home/$SEEDUSER/local/$line
		echo -e "	${GREEN}--> Le dossier ${NC}${YELLOW}$line${NC}${GREEN} a été ajouté avec succès !${NC}"
		done
		chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/local
		chmod -R 755 /home/$SEEDUSER/local
	else
		mkdir -p /home/$SEEDUSER/Medias
		echo -e " ${BWHITE}--> Création des dossiers Medias ${NC}"
		echo ""
		echo -e " ${YELLOW}--> ### Veuillez patienter, création en cours des dossiers sur Gdrive ### ${NC}"
		for media in $(cat $MEDIAVAILABLE);
		do
			service=$(echo $media | cut -d\- -f1)
			desc=$(echo $media | cut -d\- -f2)
			echo "$service $desc off" >> /tmp/menumedia.txt
		done
		MEDIASTOINSTALL=$(whiptail --title "Gestion des dossiers Medias" --checklist \
		"Medias à ajouter pour $SEEDUSER (Barre espace pour la sélection)" 28 60 17 \
		$(cat /tmp/menumedia.txt) 3>&1 1>&2 2>&3)
		MEDIASPERUSER="$MEDIASUSER$SEEDUSER"
		touch $MEDIASPERUSER
		for MEDDOCKER in $MEDIASTOINSTALL
		do
			echo -e "	${GREEN}* $(echo $MEDDOCKER | tr -d '"')${NC}"
			echo $(echo ${MEDDOCKER} | tr -d '"') >> $MEDIASPERUSER
		done
		for line in $(cat $MEDIASPERUSER);
		do
		line=$(echo $line | sed 's/\(.\)/\U\1/')
		mkdir -p /home/$SEEDUSER/local/$line
		mkdir -p /mnt/rclone/$SEEDUSER/$line 
		done
		chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/local
		chmod -R 755 /home/$SEEDUSER/local
		rm /tmp/menumedia.txt
	fi
	echo ""
}

function replace_media_compose() {
	MEDIASPERUSER="$MEDIASUSER$SEEDUSER"
	if [[ -e "$MEDIASPERUSER" ]]; then
		FILMS=$(grep -E 'Films' $MEDIASPERUSER)
		SERIES=$(grep -E 'Series' $MEDIASPERUSER)
		ANIMES=$(grep -E 'Animes' $MEDIASPERUSER)
		MUSIC=$(grep -E 'Musiques' $MEDIASPERUSER)
		EMISSIONS=$(grep -E 'Emissions' $MEDIASPERUSER)
		DOCUMENTAIRES=$(grep -E 'Documentaires' $MEDIASPERUSER)
	else
		FILMS=$(grep -E 'Films' /tmp/menumedia.txt)
		SERIES=$(grep -E 'Series' /tmp/menumedia.txt)
		ANIMES=$(grep -E 'Animes' /tmp/menumedia.txt)
		MUSIC=$(grep -E 'Musiques' /tmp/menumedia.txt)
		EMISSIONS=$(grep -E 'Emissions' /tmp/menumedia.txt)
		DOCUMENTAIRES=$(grep -E 'Documentaires' /tmp/menumedia.txt)
	fi
}

function add_user_htpasswd() {
	HTFOLDER="$CONFDIR/passwd/"
	mkdir -p $CONFDIR/passwd
	HTTEMPFOLDER="/tmp/"
	HTFILE=".htpasswd-$SEEDUSER"
	if [[ $1 == "" ]]; then
		echo ""
		echo -e "${BLUE}## HTPASSWD MANAGER ##${NC}"
		HTUSER=$(whiptail --title "HTUser" --inputbox "Enter username for htaccess" 10 60 3>&1 1>&2 2>&3)
		HTPASSWORD=$(whiptail --title "HTPassword" --passwordbox "Enter password" 10 60 3>&1 1>&2 2>&3)
	else
		HTUSER=$1
		HTPASSWORD=$2
	fi
	if [[ ! -f $HTFOLDER$HTFILE ]]; then
		htpasswd -c -b $HTTEMPFOLDER$HTFILE $HTUSER $HTPASSWORD > /dev/null 2>&1
	else
		htpasswd -b $HTFOLDER$HTFILE $HTUSER $HTPASSWORD > /dev/null 2>&1
	fi
	valid_htpasswd
}

function docker_compose() {
	echo -e "${BLUE}### DOCKERCOMPOSE ###${NC}"
	ACTDIR="$PWD"
	cd /home/$SEEDUSER/
	echo -e " ${BWHITE}* Docker-composing, Merci de patienter...${NC}"
	export COMPOSE_HTTP_TIMEOUT=600
	docker-compose up -d > /dev/null 2>&1
	checking_errors $?
	echo ""
	cd $ACTDIR
	config_post_compose
}

decompte() {
    i=$1
    while [[ $i -ge 0 ]]
      do
        echo -e "\033[1;37m\r * "$var ""$i""s" \c\033[0m"
        sleep 1
        i=$(expr $i - 1)
    done
    echo -e ""
}

function plex_sections() {
			echo -e "${BLUE}### CREATION DES BIBLIOTHEQUES PLEX ###${NC}"
			replace_media_compose
			##compteur
			var="Sections en cours de création, patientez..."
			decompte 15

			## création des bibliothèques plex
			for x in $(cat $MEDIASPERUSER);
			do
				if [[ "$x" == "$ANIMES" ]]; then
					docker exec plex-$SEEDUSER /usr/lib/plexmediaserver/Plex\ Media\ Scanner --add-section $x --type 2 --location /data/$x --lang fr
					echo -e "	${BWHITE}* $x ${NC}"
				
				elif [[ "$x" == "$SERIES" ]]; then
					docker exec plex-$SEEDUSER /usr/lib/plexmediaserver/Plex\ Media\ Scanner --add-section $x --type 2 --location /data/$x --lang fr
					echo -e "	${BWHITE}* $x ${NC}"

				elif [[ "$x" == "$EMISSIONS" ]]; then
					docker exec plex-$SEEDUSER /usr/lib/plexmediaserver/Plex\ Media\ Scanner --add-section $x --type 2 --location /data/$x --lang fr
					echo -e "	${BWHITE}* $x ${NC}"
				
				elif [[ "$x" == "$MUSIC" ]]; then
					docker exec plex-$SEEDUSER /usr/lib/plexmediaserver/Plex\ Media\ Scanner --add-section $x --type 8 --location /data/$x --lang fr
					echo -e "	${BWHITE}* $x ${NC}"
				else
					docker exec plex-$SEEDUSER /usr/lib/plexmediaserver/Plex\ Media\ Scanner --add-section $x --type 1 --location /data/$x --lang fr
					echo -e "	${BWHITE}* $x ${NC}"
				fi
			done

			## configuration plex_autoscan
			cd /home/$SEEDUSER
			sed -i '/PATH/d' docker-compose.yml
			docker-compose rm -fs plex-$SEEDUSER > /dev/null 2>&1 && docker-compose up -d plex-$SEEDUSER > /dev/null 2>&1
			checking_errors $?
			echo""
			##compteur
			var="Plex est en cours de configuration, patientez..."
			decompte 15
			checking_errors $?
			echo""
			install_plex_autoscan
			mv /home/$SEEDUSER/scripts/plex_autoscan/config/default.config /home/$SEEDUSER/scripts/plex_autoscan/config/config.json
			sed -i 's/\/var\/lib\/plexmediaserver/\/config/g' /home/$SEEDUSER/scripts/plex_autoscan/config/config.json
			sed -i 's/"DOCKER_NAME": ""/"DOCKER_NAME": "plex-'$SEEDUSER'"/g' /home/$SEEDUSER/scripts/plex_autoscan/config/config.json
			sed -i 's/"USE_DOCKER": false/"USE_DOCKER": true/g' /home/$SEEDUSER/scripts/plex_autoscan/config/config.json
			/home/$SEEDUSER/scripts/plex_autoscan/scan.py sections > /dev/null 2>&1
			/home/$SEEDUSER/scripts/plex_autoscan/scan.py sections > plex.log

			## Récupération du token de plex
			echo -e " ${BWHITE}* Récupération du token Plex${NC}"
			docker exec -ti plex-$SEEDUSER grep -E -o "PlexOnlineToken=.{0,22}" /config/Library/Application\ Support/Plex\ Media\ Server/Preferences.xml > /home/$SEEDUSER/token.txt
			TOKEN=$(grep PlexOnlineToken /home/$SEEDUSER/token.txt | cut -d '=' -f2 | cut -c2-21)
			checking_errors $?
			for i in `seq 1 50`;
			do
   				var=$(grep "$i: " plex.log | cut -d: -f2 | cut -d ' ' -f2-3)
   				if [ -n "$var" ]
   				then
     				echo "$i" "$var"
   				fi 
			done > categories.log
			PLEXCANFILE="/home/$SEEDUSER/scripts/plex_autoscan/config/config.json"
			cat "$BASEDIR/includes/config/plex_autoscan/config.json" > $PLEXCANFILE

			ID_FILMS=$(grep -E 'Films' categories.log | cut -d: -f1 | cut -d ' ' -f1)
			ID_SERIES=$(grep -E 'Series' categories.log | cut -d: -f1 | cut -d ' ' -f1)
			ID_ANIMES=$(grep -E 'Animes' categories.log | cut -d: -f1 | cut -d ' ' -f1)
			ID_MUSIC=$(grep -E 'Musiques' categories.log | cut -d: -f1 | cut -d ' ' -f1)

			if [[ -f "$SCANPORTPATH" ]]; then
				declare -i PORT=$(cat $SCANPORTPATH | tail -1)
			else
				declare -i PORT=3470
			fi

			sed -i "s|%TOKEN%|$TOKEN|g" $PLEXCANFILE
			sed -i "s|%PORT%|$PORT|g" $PLEXCANFILE
			sed -i "s|%FILMS%|$FILMS|g" $PLEXCANFILE
			sed -i "s|%SERIES%|$SERIES|g" $PLEXCANFILE
			sed -i "s|%MUSIC%|$MUSIC|g" $PLEXCANFILE
			sed -i "s|%ANIMES%|$ANIMES|g" $PLEXCANFILE
			sed -i "s|%ID_FILMS%|$ID_FILMS|g" $PLEXCANFILE
			sed -i "s|%ID_SERIES%|$ID_SERIES|g" $PLEXCANFILE
			sed -i "s|%ID_ANIMES%|$ID_ANIMES|g" $PLEXCANFILE
			sed -i "s|%ID_MUSIC%|$ID_MUSIC|g" $PLEXCANFILE
			sed -i "s|%SEEDUSER%|$SEEDUSER|g" $PLEXCANFILE
			echo ""

			## installation plex_dupefinder
			install_plex_dupefinder

			## récupération nom de domaine
			for line in $(cat $INSTALLEDFILE);
			do
				NOMBRE=$(sed -n "/$SEEDUSER/=" $CONFDIR/users)
				if [ $NOMBRE -le 1 ] ; then
					ACCESSDOMAIN=$(grep plex $INSTALLEDFILE | cut -d\- -f3)
				else
					ACCESSDOMAIN=$(grep plex $INSTALLEDFILE | cut -d\- -f3-4)
				fi
			done
			
			## intégration des variables
			PLEXDUPEFILE="/home/$SEEDUSER/scripts/plex_dupefinder/config.json"
			cat "$BASEDIR/includes/config/plex_dupefinder/config.json" > $PLEXDUPEFILE
			sed -i "s|%ID_FILMS%|$ID_FILMS|g" $PLEXDUPEFILE
			sed -i "s|%ID_SERIES%|$ID_SERIES|g" $PLEXDUPEFILE
			sed -i "s|%FILMS%|$FILMS|g" $PLEXDUPEFILE
			sed -i "s|%SERIES%|$SERIES|g" $PLEXDUPEFILE
			sed -i "s|%TOKEN%|$TOKEN|g" $PLEXDUPEFILE
			sed -i "s|%ACCESSDOMAIN%|$ACCESSDOMAIN|g" $PLEXDUPEFILE

			## mise en place d'un cron pour le lancement de plexdupefinder
			(crontab -l | grep . ; echo "*/1 * * * * python3 /home/$SEEDUSER/scripts/plex_dupefinder/plexdupes.py >> /home/$SEEDUSER/scripts/plex_dupefinder/activity.log") | crontab -
			
			## lancement plex_autoscan
			# chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/scripts/plex_autoscan
			systemctl start plex_autoscan-$SEEDUSER.service > /dev/null 2>&1
			checking_errors $?
			PORT=$PORT+1
			echo $PORT >> $SCANPORTPATH
			chown -R $SEEDUSER:$SEEDGROUP /home/$SEEDUSER/scripts
			rm /home/$SEEDUSER/token.txt
			echo ""
			install_cloudplow
}

function valid_htpasswd() {
	if [[ -d "$CONFDIR" ]]; then
		HTFOLDER="$CONFDIR/passwd/"
		mkdir -p $HTFOLDER
		HTTEMPFOLDER="/tmp/"
		HTFILE=".htpasswd-$SEEDUSER"
		cat "$HTTEMPFOLDER$HTFILE" >> "$HTFOLDER$HTFILE"
		VAR=$(sed -e 's/\$/\$$/g' "$HTFOLDER$HTFILE")
		cd $HTFOLDER
		touch login
		echo "$HTUSER $HTPASSWORD" >> "$HTFOLDER/login"
		rm "$HTTEMPFOLDER$HTFILE"
	fi
}

function add_app_htpasswd() {
	if [[ -d "$CONFDIR" ]]; then
		HTFOLDER="$CONFDIR/passwd/"
		HTFILE=".htpasswd-$SEEDUSER"
		VAR=$(sed -e 's/\$/\$$/g' "$HTFOLDER$HTFILE")
	fi
}

function manage_users() {
	echo -e "${BLUE}##########################################${NC}"
	echo -e "${BLUE}###      Gestions des Utilisateurs     ###${NC}"
	echo -e "${BLUE}##########################################${NC}"
	echo ""
	MANAGEUSER=$(whiptail --title "Management" --menu \
	                "Choisir une action" 10 45 2 \
	                "1" "Nouvelle Seedbox Utilisateur" \
	                "2" "Supprimer Seedbox Utilisateur" 3>&1 1>&2 2>&3)
			[[ "$?" = 1 ]] && script_plexdrive;
	case $MANAGEUSER in
		"1" )
			PLEXDRIVE="/usr/bin/plexdrive"
			echo -e "${GREEN}###   NOUVELLE SEEDBOX USER   ###${NC}"
			echo -e "${GREEN}---------------------------------${NC}"
			echo ""
			define_parameters
			add_ftp > /dev/null 2>&1
			if [[ -e "$PLEXDRIVE" ]]; then
				rclone_service
				choose_media_folder_plexdrive
				unionfs_fuse
				pause
				choose_services
				install_services
				docker_compose
				CLOUDPLOWFOLDER="/home/$SEEDUSER/scripts/cloudplow"
				if [[ ! -d "$CLOUDPLOWFOLDER" ]]; then
				install_cloudplow
				sed -i "s/\"enabled\"\: true/\"enabled\"\: false/g" /home/$SEEDUSER/scripts/cloudplow/config.json
				fi
				resume_seedbox
				pause
				script_plexdrive
			else
				choose_media_folder_classique
				choose_services
				install_services
				docker_compose
				resume_seedbox
				pause
				script_classique
			fi
			;;

		"2" )
			echo -e "${GREEN}###   SUPRESSION SEEDBOX USER   ###${NC}"
			echo -e "${GREEN}-----------------------------------${NC}"
			echo ""
			PLEXDRIVE="/usr/bin/plexdrive"
			TMPGROUP=$(cat $GROUPFILE)
			TABUSERS=()
			for USERSEED in $(members $TMPGROUP)
			do
	        		IDSEEDUSER=$(id -u $USERSEED)
	        		TABUSERS+=( ${USERSEED//\"} ${IDSEEDUSER//\"} )
			done
			## CHOOSE USER
			SEEDUSER=$(whiptail --title "Gestion des Applis" --menu \
	                		"Merci de sélectionner l'Utilisateur" 12 50 3 \
	                		"${TABUSERS[@]}"  3>&1 1>&2 2>&3)
			[[ "$?" = 1 ]] && script_plexdrive;
			## RESUME USER INFORMATIONS
			USERDOCKERCOMPOSEFILE="/home/$SEEDUSER/docker-compose.yml"
			USERRESUMEFILE="/home/$SEEDUSER/resume"
			cd /home/$SEEDUSER
			echo -e "${BLUE}### SUPPRESSION CONTAINERS ###${NC}"
			checking_errors $?
			docker-compose rm -fs > /dev/null 2>&1
			echo ""
			if [[ -e "$PLEXDRIVE" ]]; then
				echo -e "${BLUE}### SUPPRESSION USER RCLONE/PLEXDRIVE ###${NC}"
				PLEXAUTOSCAN="/etc/systemd/system/plex_autoscan-$SEEDUSER.service"
				if [[ -e "$PLEXAUTOSCAN" ]]; then
					systemctl stop plex_autoscan-$SEEDUSER.service
					systemctl disable plex_autoscan-$SEEDUSER.service > /dev/null 2>&1
					rm /etc/systemd/system/plex_autoscan-$SEEDUSER.service
				fi
				systemctl stop rclone-$SEEDUSER.service
				systemctl disable rclone-$SEEDUSER.service > /dev/null 2>&1
				rm /etc/systemd/system/rclone-$SEEDUSER.service
				systemctl stop cloudplow-$SEEDUSER.service
				systemctl disable cloudplow-$SEEDUSER.service > /dev/null 2>&1
				rm /etc/systemd/system/cloudplow-$SEEDUSER.service
				systemctl stop unionfs-$SEEDUSER.service
				systemctl disable unionfs-$SEEDUSER.service > /dev/null 2>&1
				rm /etc/systemd/system/unionfs-$SEEDUSER.service
				checking_errors $?
				echo""
			fi
		        echo -e "${BLUE}### SUPPRESSION USER ###${NC}"
			rm -rf /home/$SEEDUSER > /dev/null 2>&1
			userdel -rf $SEEDUSER > /dev/null 2>&1
			sed -i "/$SEEDUSER/d" $CONFDIR/users
			rm $CONFDIR/passwd/.htpasswd-$SEEDUSER
			sed -n -i "/$SEEDUSER/!p" $CONFDIR/passwd/login
			checking_errors $?
			echo""
			echo -e "${BLUE}### $SEEDUSER a été supprimé ###${NC}"
			echo ""
			pause
			if [[ -e "$PLEXDRIVE" ]]; then
				script_plexdrive
			else
				script_classique
			fi
			;;
	esac
}

function manage_apps() {
	echo -e "${BLUE}##########################################${NC}"
	echo -e "${BLUE}###          GESTION DES APPLIS        ###${NC}"
	echo -e "${BLUE}##########################################${NC}"
	TMPGROUP=$(cat $GROUPFILE)
	TABUSERS=()
	for USERSEED in $(members $TMPGROUP)
	do
	        IDSEEDUSER=$(id -u $USERSEED)
	        TABUSERS+=( ${USERSEED//\"} ${IDSEEDUSER//\"} )
	done
	## CHOISIR USER
	SEEDUSER=$(whiptail --title "App Manager" --menu \
	                "Merci de sélectionner l'Utilisateur" 12 50 3 \
	                "${TABUSERS[@]}"  3>&1 1>&2 2>&3)
			[[ "$?" = 1 ]] && script_plexdrive;
	
	## INFORMATIONS UTILISATEUR
	USERDOCKERCOMPOSEFILE="/home/$SEEDUSER/docker-compose.yml"
	USERRESUMEFILE="/home/$SEEDUSER/resume"
	echo ""
	echo -e "${GREEN}### Gestion des Applis pour: $SEEDUSER ###${NC}"
	echo -e " ${BWHITE}* Docker-Compose file: $USERDOCKERCOMPOSEFILE${NC}"
	echo -e " ${BWHITE}* Resume file: $USERRESUMEFILE${NC}"
	echo ""
	## CHOOSE AN ACTION FOR APPS
	ACTIONONAPP=$(whiptail --title "App Manager" --menu \
	                "Selectionner une action :" 12 50 3 \
	                "1" "Ajout Docker Applis"  \
	                "2" "Supprimer une Appli" 3>&1 1>&2 2>&3)
			[[ "$?" != 0 ]] && script_plexdrive;
	case $ACTIONONAPP in
		"1" ) ## Ajout APP
			CURRTIMEZONE=$(cat /etc/timezone)
			TIMEZONEDEF=$(whiptail --title "Timezone" --inputbox \
			"Merci de vérifier votre timezone" 7 66 "$CURRTIMEZONE" \
			3>&1 1>&2 2>&3)
			if [[ $TIMEZONEDEF == "" ]]; then
				TIMEZONE=$CURRTIMEZONE
			else
				TIMEZONE=$TIMEZONEDEF
			fi
			choose_services
			add_app_htpasswd
			install_services
			docker_compose
			resume_seedbox
			pause
			if [[ -e "$PLEXDRIVE" ]]; then
				script_plexdrive
			else
				script_classique
			fi
			;;
		"2" ) ## Suppression APP
			echo -e " ${BWHITE}* Edit my app${NC}"
			TABSERVICES=()
			for SERVICEACTIVATED in $(cat $USERRESUMEFILE)
			do
			        SERVICE=$(echo $SERVICEACTIVATED | cut -d\- -f1)
			        PORT=$(echo $SERVICEACTIVATED | cut -d\- -f2)
			        TABSERVICES+=( ${SERVICE//\"} ${PORT//\"} )
			done
			APPSELECTED=$(whiptail --title "App Manager" --menu \
			              "Sélectionner l'Appli à supprimer" 19 45 11 \
			              "${TABSERVICES[@]}"  3>&1 1>&2 2>&3)
			echo -e " ${GREEN}   * $APPSELECTED${NC}"
			[[ "$?" = 1 ]] && script_plexdrive;
			cd /home/$SEEDUSER
			docker-compose rm -fs "$APPSELECTED"-"$SEEDUSER"
			sed -i "/#START"$APPSELECTED"#/,/#END"$APPSELECTED"#/d" /home/$SEEDUSER/docker-compose.yml
			sed -i "/$APPSELECTED/d" /home/$SEEDUSER/resume
			rm -rf /home/$SEEDUSER/docker/$APPSELECTED
			checking_errors $?
			echo""
			echo -e "${BLUE}### $APPSELECTED a été supprimé ###${NC}"
			echo ""
			pause
			if [[ -e "$PLEXDRIVE" ]]; then
				script_plexdrive
			else
				script_classique
			fi
			;;	
	esac
}

function resume_seedbox() {
	echo -e "${BLUE}##########################################${NC}"
	echo -e "${BLUE}###     INFORMATION SEEDBOX INSTALL    ###${NC}"
	echo -e "${BLUE}##########################################${NC}"
	echo -e " ${BWHITE}* Accès Applis à partir de URL :${NC}"
	for line in $(cat $INSTALLEDFILE);
	do
		NOMBRE=$(sed -n "/$SEEDUSER/=" $CONFDIR/users)
		if [ $NOMBRE -le 1 ] ; then
			ACCESSDOMAIN=$(echo $line | cut -d\- -f3)
			DOCKERAPP=$(echo $line | cut -d\- -f1)
			echo -e "	--> ${BWHITE}$DOCKERAPP${NC} --> ${YELLOW}$ACCESSDOMAIN${NC}"
		else
			ACCESSDOMAIN=$(echo $line | cut -d\- -f3-4)
			DOCKERAPP=$(echo $line | cut -d\- -f1)
			echo -e "	--> ${BWHITE}$DOCKERAPP${NC} --> ${YELLOW}$ACCESSDOMAIN${NC}"
		fi
	done
	IDENT="$CONFDIR/passwd/.htpasswd-$SEEDUSER"	
	if [[ ! -d $IDENT ]]; then
		PASSE=$(grep $SEEDUSER $CONFDIR/passwd/login | cut -d ' ' -f2)
		echo ""
		echo -e " ${BWHITE}* Vos IDs :${NC}"
		echo -e "	--> Utilisateur: ${YELLOW}$SEEDUSER${NC}"
		echo -e "	--> Password: ${YELLOW}$PASSE${NC}"
		echo ""
	else
		echo -e " ${BWHITE}* Here is your IDs :${NC}"
		echo -e "	--> Utilisateur: ${YELLOW}$HTUSER${NC}"
		echo -e "	--> Password: ${YELLOW}$HTPASSWORD${NC}"
		echo ""
		echo ""
	fi
	rm -Rf $SERVICESPERUSER > /dev/null 2>&1
}

function uninstall_seedbox() {
	clear
	echo -e "${BLUE}##########################################${NC}"
	echo -e "${BLUE}###       DESINSTALLATION SEEDBOX      ###${NC}"
	echo -e "${BLUE}##########################################${NC}"
	SEEDGROUP=$(cat $GROUPFILE)
	PLEXDRIVE="/usr/bin/plexdrive"
	if [[ -e "$PLEXDRIVE" ]]; then
		echo -e " ${BWHITE}* Suppression Plexdrive/rclone...${NC}"
		service rclone stop
		service plexdrive stop
		rm /etc/systemd/system/rclone.service
		rm /etc/systemd/system/plexdrive.service
		rm /usr/bin/rclone
		rm -rf /mnt/plexdrive
		rm -rf /mnt/rclone
		rm -rf /root/.plexdrive
		rm -rf /root/.config/rclone
		checking_errors $?
	fi
	for seeduser in $(cat $USERSFILE)
	do
		USERHOMEDIR="/home/$seeduser"
		PLEXAUTOSCAN="/etc/systemd/system/plex_autoscan-$seeduser.service"
		echo -e " ${BWHITE}* Suppression users $seeduser...${NC}"
		if [[ -e "$PLEXDRIVE" ]]; then
				if [[ -e "$PLEXAUTOSCAN" ]]; then
					systemctl stop plex_autoscan-$seeduser.service
					systemctl disable plex_autoscan-$seeduser.service > /dev/null 2>&1
					rm /etc/systemd/system/plex_autoscan-$seeduser.service
				fi
			systemctl stop rclone-$SEEDUSER.service
			systemctl disable rclone-$SEEDUSER.service > /dev/null 2>&1
			rm /etc/systemd/system/rclone-$SEEDUSER.service
			systemctl stop cloudplow-$seeduser.service
			systemctl disable cloudplow-$seeduser.service > /dev/null 2>&1
			rm /etc/systemd/system/cloudplow-$seeduser.service
			service unionfs-$seeduser stop
			systemctl disable unionfs-$SEEDUSER.service > /dev/null 2>&1
			rm /etc/systemd/system/unionfs-$seeduser.service
		fi
		userdel -rf $seeduser > /dev/null 2>&1
		checking_errors $?
		echo -e " ${BWHITE}* Suppression home $seeduser...${NC}"
		rm -Rf $USERHOMEDIR
		checking_errors $?
		echo ""
	done
	rm /usr/bin/plexdrive > /dev/null 2>&1
	echo -e " ${BWHITE}* Suppression Containers...${NC}"
	docker rm -f $(docker ps -aq) > /dev/null 2>&1
	checking_errors $?
	echo -e " ${BWHITE}* Suppression group...${NC}"
	groupdel $SEEDGROUP > /dev/null 2>&1
	checking_errors $?
	echo -e " ${BWHITE}* Removing Seedbox-compose directory...${NC}"
	rm -Rf $CONFDIR
	checking_errors $?
	cd /opt && rm -Rf seedbox-compose
	pause
}

function pause() {
	echo ""
	echo -e "${YELLOW}###  -->APPUYER SUR ENTREE POUR CONTINUER<--  ###${NC}"
	read
	echo ""
}
